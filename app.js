var img = "img/i";
var jpg = ".jpg";

var ishihara = "";
var tipoPergunta = [];

function manipulaPergunta(contador, perguntas){
	$('#imgTeste').remove();
	$('#pergunta').remove();
	$('#resposta').remove();
	
	$('#voltar').after('<img src=' + ishihara[perguntas[contador]][0] + ' alt="teste" id="imgTeste">');
	$('#imgTeste').after('<p id="pergunta">' + ishihara[perguntas[contador]][2] + '</p>');
	$('#pergunta').after('<input type="number" id="resposta" value="0">');
}


function teste(perguntas, url){
	var completo =  false;
	var contador = 0;
	var erros = 0;
	var tamanho = perguntas.length;
	
	$('#voltar').after('<img src=' + ishihara[perguntas[contador]][0] + ' alt="teste" id="imgTeste">');
	$('#imgTeste').after('<p id="pergunta">' + ishihara[perguntas[contador]][2] + '</p>');
	$('#pergunta').after('<input type="number" id="resposta" value="0">');
	
	$(document).keydown(function(objEvent) {
		if (objEvent.keyCode == 13) {
			$('#avancar').click();
		}
	});

	
	
	$('#avancar').click(function() {
		
		if(completo === false){
			if(parseInt($('#resposta').val()) !== ishihara[perguntas[contador]][1]){
				erros++;
			}
			
			contador++;	
			
			if(contador < 10){
				manipulaPergunta(contador, perguntas);			
			}else{
				completo = true;
				$('#avancar').click();
			}
		}else{
			$('img').remove();
			$('#voltar').remove();
			$('#pergunta').remove();
			$('#resposta').remove();
			$('#resultado').remove();
			$('#avancar').remove();
			
			if(erros == 0){
				if(url.indexOf("lang=pt-br") !== -1){
					$('body').append('<h2 class="resultado" id="resultado">' + testeResultado[0][0] + '</h2>');
					$('body').append('<a href="home.html?lang=pt-br" class="bottom" id="fim">' + testeResultado[1][0] + '</a>');
				}else{
					$('body').append('<h2 class="resultado" id="resultado">' + testeResultado[0][1] + '</h2>');
					$('body').append('<a href="home.html?lang=en" class="bottom" id="fim">' + testeResultado[1][1] + '</a>');
				}
			}else{
				if(url.indexOf("lang=pt-br") !== -1){
					$('body').append('<h2 class="resultado" id="resultado">' + testeResultado[2][0] + 
									 (erros*10) + '%!<br><br>' + testeResultado[3][0] + '</h2>');
					$('body').append('<a href="hospital.html?lang=pt-br" class="botResultado" id="sim">' + testeResultado[4][0] + '</a>');
					$('body').append('<a href="home.html?lang=pt-br" class="botResultado" id="nao">' + testeResultado[5][0] + '</a>');
				}else{
					$('body').append('<h2 class="resultado" id="resultado">' + testeResultado[2][1] + 
									 (erros*10) + '%!<br><br>' + testeResultado[3][1] + '</h2>');
					$('body').append('<a href="hospital.html?lang=en" class="botResultado" id="sim">' + testeResultado[4][1] + '</a>');
					$('body').append('<a href="home.html?lang=en" class="botResultado" id="nao">' + testeResultado[5][1] + '</a>');
				}
			}
		}
	});
}

 $(document).ready(function() {
	var auxiliar = 0
	var perguntas = [];
	var sorteador = 0;
	var tipo  =  "";
	
	var url = document.URL;
	
	if(url.indexOf("lang=pt-br") !== -1){
		tipoPergunta[0] = testePergunta[0][0];
		tipoPergunta[1] = testePergunta[1][0];
		
		$("#voltar").attr("href", "home.html?lang=pt-br");
		
		$("#voltar").text(testeBotoes[0][0]);
		$("#avancar").text(testeBotoes[1][0]);
	}else{
		tipoPergunta[0] = testePergunta[0][1];
		tipoPergunta[1] = testePergunta[1][1];
		
		$("#voltar").attr("href", "home.html?lang=en");
		
		$("#voltar").text(testeBotoes[0][1]);
		$("#avancar").text(testeBotoes[1][1]);
	}
	
	ishihara = [[img + "01" + jpg, 12, tipoPergunta[0]], 
				[img + "02" + jpg, 8, tipoPergunta[0]], 
				[img + "03" + jpg, 6, tipoPergunta[0]], 
				[img + "04" + jpg, 29, tipoPergunta[0]], 
				[img + "05" + jpg, 57, tipoPergunta[0]], 
				[img + "06" + jpg, 5, tipoPergunta[0]], 
				[img + "07" + jpg, 3, tipoPergunta[0]], 
				[img + "08" + jpg, 15, tipoPergunta[0]], 
				[img + "09" + jpg, 74, tipoPergunta[0]], 
				[img + "10" + jpg, 2, tipoPergunta[0]], 
				[img + "11" + jpg, 6, tipoPergunta[0]], 
				[img + "12" + jpg, 97, tipoPergunta[0]], 
				[img + "13" + jpg, 45, tipoPergunta[0]], 
				[img + "14" + jpg, 5, tipoPergunta[0]], 
				[img + "15" + jpg, 7, tipoPergunta[0]], 
				[img + "16" + jpg, 16, tipoPergunta[0]], 
				[img + "17" + jpg, 73, tipoPergunta[0]], 
				[img + "18" + jpg, 0, tipoPergunta[0]], 
				[img + "19" + jpg, 0, tipoPergunta[0]], 
				[img + "20" + jpg, 0, tipoPergunta[0]], 
				[img + "21" + jpg, 0, tipoPergunta[0]], 
				[img + "22" + jpg, 26, tipoPergunta[0]], 
				[img + "23" + jpg, 42, tipoPergunta[0]], 
				[img + "24" + jpg, 35, tipoPergunta[0]], 
				[img + "25" + jpg, 96, tipoPergunta[0]], 
				[img + "26" + jpg, 2, tipoPergunta[1]], 
				[img + "27" + jpg, 2, tipoPergunta[1]], 
				[img + "28" + jpg, 0, tipoPergunta[1]], 
				[img + "29" + jpg, 0, tipoPergunta[1]], 
				[img + "30" + jpg, 1, tipoPergunta[1]],
				[img + "31" + jpg, 1, tipoPergunta[1]],
				[img + "32" + jpg, 1, tipoPergunta[1]],
				[img + "33" + jpg, 1, tipoPergunta[1]],
				[img + "34" + jpg, 1, tipoPergunta[1]],
				[img + "35" + jpg, 1, tipoPergunta[1]],
				[img + "36" + jpg, 1, tipoPergunta[1]],
				[img + "37" + jpg, 1, tipoPergunta[1]], 
				[img + "38" + jpg, 1, tipoPergunta[1]]];
	
	
	while(sorteador !== 10){
		do{
			auxiliar =  Math.floor(Math.random() * 38);
		}while(perguntas.indexOf(auxiliar) !== -1)
		
		perguntas[sorteador] = auxiliar;
		
		sorteador++;
	}
	  
	teste(perguntas, url);
 });
