var index = [['Teste','Test'],['Sobre','About']];

var testeBotoes = [['Voltar','Back'],['Avançar','Next']];

var testePergunta = [
						[
							'<strong>Qual número você vê na imagem?</strong> (Deixe zero caso não identifique nenhum número)', 
							'<strong>What number you see in the picture?</strong> (Let zero if you do not identify any number)'
						],
						[
							'<strong>Quantas linhas você consegue ver na imagem?</strong>',
							'<strong>How many lines can you see in the picture?</strong>'
						]
					];
					
var testeResultado = [
						['Parabéns, nossos testes apontam que seu índice de daltonismo é de 0%!','Congratulations, our tests show that your blindness rate is 0%!'],
						['Ótimo! :D', 'Perfect! :D'],
						['Nossos testes apontam que seu índice de daltonismo é de ', 'Our tests indicate that your blindness rate is '],
						['Você gostaria de pesquisar por hospitais e postos de saúde próximos?', 'You would like to search for hospitals and health centers nearby?'],
						['Sim','Yes'],
						['Não','No']
					];
					
var sobre = [
				['Desenvolvedor', 'Developer'],
				['O que é daltonismo?', 'What is color blindness?'],
				[
				 'O daltonismo (também chamado de discromatopsia ou discromopsia) é uma perturbação da percepção visual caracterizada pela incapacidade de diferenciar todas ou algumas cores, manifestando-se muitas vezes pela dificuldade em distinguir o verde do vermelho. Esta perturbação tem normalmente origem genética, mas pode também resultar de lesão nos órgãos responsáveis pela visão, ou de lesão de origem neurológica.',
				 'Color blindness, or color vision deficiency, is the inability or decreased ability to see color, or perceive color differences, under normal lighting conditions. Color blindness affects a significant percentage of the population.[1] There is no actual blindness but there is a deficiency of color vision.'
				 ]
			];


var hospital = [
				['FIM','END'],
				['Lista de hospitais e postos de Saúde próximos:','List of hospitals and health centers nearby:']
				];


var curiosidades = [
					[
					 '99% de todas as pessoas daltônicas não são realmente daltônicas, apenas tem uma pequena deficiência para identificar cores, o termo daltonismo é enganador.',
					 '99% of all colorblind people are not really color blind but color deficient; the term color blindness is misleading.'
					],
					[
					 'O daltonismo vermelho-verde é uma combinação do daltonismo vermelho e daltonismo verde.',
					 'Red-green color blindness is a combination of red-blindness (protan defects) and green-blindness (deutan defects).'
					],
					[
					 'O daltonismo é mais comum entre homens do que entre mulheres, porque a forma mais comum de deficiência de visão de cores é codificado no cromossomo X.',
					 'Color blindness is more prevalent among males than females, because the most common form of color vision deficiency is encoded on the X sex chromosome.'
					],
					[
					 '"Que cor é essa?" é a pergunta mais chata que você pode fazer a um amigo daltônico.',
					 '"What color is this?" is the most annoying question you can ask your colorblind friend.'
					],
					[
					 'Cerca de 8% de todos os homens ao redor do mundo possuem algum grau de daltonismo.',
					 'About 8% of all men are suffering from color blindness.'
					],
					[
					 'Cerca de 0.5% de todas as mulheres ao redor do mundo possuem algum grau de daltonismo.',
					 'About 0.5% of all woman are suffering from color blindness.'
					],
					[
					 'Se uma mulher for daltonica vermelho-verde, todos os seus filhos também serão',
					 'If a woman is red-green colorblind, all her sons will also be colorblind.'
					],
					[
					 'Não existe tratamento ou cura para o daltonismo',
					 'There is no treatment or cure for color blindness.'
					]
				]

var curiosidadesTitulo = ['Curiosidades', 'Trivia'];
